console.log("LOOPING PERTAMA");
var loop = 0;

while (loop < 20) {
  loop += 2;
  console.log(loop + " - I love coding");
}

console.log("LOOPING KEDUA");

while (loop > 0) {
  console.log(loop + " - I will become a mobile developer");
  loop -= 2;
}

console.log("Looping menggunakan for");
for (angka = 1; angka <= 20; angka++) {
  if (angka >= 3 && angka % 3 === 0 && angka % 2 !== 0) {
    console.log(angka + " I Love Coding");
  } else if (angka % 2 === 1) {
    console.log(angka + " Santai");
  } else {
    console.log(angka + " Berkualitas");
  }
}

console.log("No. 3 Membuat Persegi Panjang #");
var loop = 0;

while (loop < 4) {
  loop += 1;
  console.log("########");
}

console.log("No. 4 Membuat Tangga ");
var loop = 0;

while (loop < 7) {
  loop += 1;
  console.log("########");
}

console.log("No. 5 Membuat Papan Catur");
var loop = 0;

while (loop < 4) {
  loop += 1;
  console.log("########");
}
